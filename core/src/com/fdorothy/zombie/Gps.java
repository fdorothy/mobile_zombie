package com.fdorothy.zombie;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import java.lang.Math;

public class Gps {
  static final double[] U = {0.8477168565441121, 0.3251719820341857, 0.4190934421235062};
  static final double[] V = {-0.5191796989609103, 0.452064913566408, 0.725320449256089};
  static final double[] O = {0.04636708998278531, -0.8325203948697296, 0.5520506182339422};
  static final double a = 53980993.9699;
  static final double b = 54997158.5965;
  static double[] tmp = new double[3];

  static Vector3 lat_lng_to_game(double lat, double lng, Vector3 out) {
    lat_lng_to_utm(lat, lng, tmp);
    utm_to_game(tmp, tmp);
    out.x = (float)tmp[0];
    out.y = (float)tmp[1];
    return out;
  }

  static void lat_lng_to_utm(double lat, double lng, double[] out) {
    double degRad = Math.PI/180.0;
    lat *= degRad;
    lng *= degRad;
    out[0] = Math.cos(lng)*Math.cos(lat);
    out[1] = Math.sin(lng)*Math.cos(lat);
    out[2] = Math.sin(lat);
  }

  static void utm_to_game(double[] utm, double[] out) {
    out[0] = utm[0] - O[0];
    out[1] = utm[1] - O[1];
    out[2] = utm[2] - O[2];
    double x = a*(out[0]*U[0]+out[1]*U[1]+out[2]*U[2]);
    double y = b*(out[0]*V[0]+out[1]*V[1]+out[2]*V[2]);
    out[0] = x;
    out[1] = y;
  }
}
