package com.fdorothy.zombie;

import com.badlogic.gdx.math.Vector3;

public class Survivor {
  public int life;
  public int ammo;
  Vector3 pos;

  public Survivor() {
    pos = new Vector3();
    pos.x = 0.0f;
    pos.y = 0.0f;
    life = 5;
    ammo = 5;
  }

  public void reset() {
    life = 5;
    ammo = 5;
    pos.set(0.0f, 0.0f, 0.0f);
  }
}
