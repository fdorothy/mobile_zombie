package com.fdorothy.zombie;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class AI {
  public Array <Zombie> zombies;
  public int idx;
  public long millis;
  public Survivor survivor;

  AI() {
    idx = 0;
    millis = TimeUtils.millis();
  }

  void update(long t) {
    if (t - millis > 100) {

      for (int i=0; i<15; i++) {
        update_zombie(idx);
        if (idx >= zombies.size-1) {
	  idx = 0;
	} else {
	  idx += 1;
	}
      }

      millis = t;
    }
  }

  void update_zombie(int idx) {
    Zombie z = zombies.get(idx);

    float rand1 = (float)MathUtils.random.nextDouble();
    float rand2 = (float)MathUtils.random.nextDouble();
    float rand3 = (float)MathUtils.random.nextDouble();

    if (z.state == 0) { // void
      z.spawn(rand1 * 153 * 32, rand2 * 43 * 32);
    } else if (z.state == 2) { // alive

      float dx = survivor.pos.x - z.pos.x;
      float dy = survivor.pos.y - z.pos.y;
      float d = z.pos.dst(survivor.pos);

      if (d > 0.0f) {
	if (d < 32.0f * 8) {
	  z.set_destination(20.0f * dx / d, 20.0f * dy / d);
	} else {
	  if (rand1 > 0.85f) {
	    dx = MathUtils.cos(rand2 * 2 * MathUtils.PI);
	    dy = MathUtils.sin(rand2 * 2 * MathUtils.PI);
	    z.set_destination(28.0f * dx, 28.0f * dy);
	  } else {
	    dx = 0.0f;
	    dy = 0.0f;
	  }
	}
      }
    }
  }
}
