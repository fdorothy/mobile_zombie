package com.fdorothy.zombie;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class GameScreen implements Screen {
  Survivor survivor;
  Array<Zombie> zombies;
  Array<Item> items;
  Map map;
  AI ai;
  long last_millis;
  long start_millis;
  static final int gps_enabled = 1;

  OrthographicCamera camera;
  OrthographicCamera camera_hud;

  final ZombieGame game;

  public GameScreen(final ZombieGame game) {
    this.game = game;

    last_millis = TimeUtils.millis();
    start_millis = last_millis;

    camera = new OrthographicCamera();
    camera.setToOrtho(false, game.res.width, game.res.height);
    camera.zoom = 1.0f;

    camera_hud = new OrthographicCamera();
    camera_hud.setToOrtho(false, game.res.width, game.res.height);

    map = new Map(game.res.map);

    survivor = new Survivor();
    zombies = new Array<Zombie>();
    for (int i=0; i<100; i++) {
      zombies.add(new Zombie(map));
    }
    items = new Array<Item>();
    for (int i=0; i<20; i++) {
      items.add(new Item(map));
    }
    ai = new AI();
    ai.zombies = zombies;
    ai.survivor = survivor;
  }
  
  @Override
  public void render (float dt) {
    last_millis = TimeUtils.millis();
    update(dt);

    Gdx.gl.glClearColor(0, 0.15f, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    draw_board(dt);
    draw_hud();
    process_input(dt);
  }

  public void draw_board(float dt) {
    camera.position.set(survivor.pos);
    camera.update();
    game.res.batch.setProjectionMatrix(camera.combined);

    map.renderer.setView(camera);
    map.renderer.render();

    game.res.batch.begin();
    draw_avatar(game.res.man, survivor.pos);

    for (int i=0; i<items.size; i++) {
      Item item = items.get(i);
      if (item.state == Item.ItemState.VISIBLE) {
	if (item.type == Item.ItemType.AMMO) {
	  draw_avatar(game.res.bullet_piece, item.pos);
	} else if (item.type == Item.ItemType.LIFE) {
	  draw_avatar(game.res.heart_piece, item.pos);
	}
      }
    }

    for (int i=0; i<100; i++) {
      Zombie z = zombies.get(i);
      if (z.state == 2) {
	draw_avatar(game.res.zombie, z.pos);
      } else if (z.state == 1) {
	//batch.draw(spawn_img, z.x, z.y);
      } else if (z.state == 3) {
	draw_avatar(game.res.dead, z.pos);
      }
    }

    game.res.batch.end();
  }

  public void draw_hud() {
    camera_hud.update();
    game.res.batch_hud.setProjectionMatrix(camera_hud.combined);

    game.res.batch_hud.begin();

    // time-alive
    int time_alive = (int)((last_millis - start_millis) / 1000);
    int seconds = (int)time_alive % 60;
    int minutes = (int)(time_alive / 60) % 60;
    int hours = (int)(time_alive / 3600);
    game.res.font.draw(game.res.batch_hud, "" + hours + ":" + minutes + ":" + seconds, game.res.width/2.0f, game.res.height-20);

    for (int i=0; i<survivor.ammo; i++) {
      game.res.batch_hud.draw(game.res.bullet, (i+1)*24, game.res.height-64);
    }

    for (int i=0; i<survivor.life; i++) {
      game.res.batch_hud.draw(game.res.heart, game.res.width-(i+2)*32, game.res.height-64);
    }

    game.res.batch_hud.end();
  }

  public void update(float dt) {
    ai.update(last_millis);
    for (int i=0; i<zombies.size; i++) {
      zombies.get(i).update(dt);
    }
    for (int i=0; i<items.size; i++) {
      items.get(i).update(dt);
    }
    man_update();
  }

  public void process_input(float dt) {
    if (Gdx.input.isKeyPressed(Keys.W)) {
      survivor.pos.y += dt * 32.0 * 4;
    }
    if (Gdx.input.isKeyPressed(Keys.S)) {
      survivor.pos.y -= dt * 32.0 * 4;
    }
    if (Gdx.input.isKeyPressed(Keys.A)) {
      survivor.pos.x -= dt * 32.0 * 4;
    }
    if (Gdx.input.isKeyPressed(Keys.D)) {
      survivor.pos.x += dt * 32.0 * 4;
    }
    if (Gdx.input.justTouched()) {
      int x = Gdx.input.getX();
      int y = Gdx.input.getY();
      Vector3 cur = camera.unproject(new Vector3((float)x, (float)y, 0.0f));
      if (survivor.ammo > 0) {
        float min_d=100.0f;
        Zombie min_z = null;
        for (int i=0; i<100; i++) {
          Zombie z = zombies.get(i);
          if (z.state == 2) {
            float d = cur.dst(z.pos);
            if (d < min_d) {
              min_d = d;
              min_z = z;
            }
          }
        }
        if (min_z != null && min_d < 32.0f) {
          min_z.shoot();
          survivor.ammo--;
        }
      }
    }
    if (gps_enabled == 1) {
      survivor.pos.x = game.gps.x;
      survivor.pos.y = game.gps.y;
      Gdx.app.log("gps", "x, y = " + survivor.pos.x + ", " + survivor.pos.y);
    }
  }
	
  @Override
  public void dispose () {
  }
  
  @Override
  public void hide() {
  }

  @Override
  public void resume() {
  }

  @Override
  public void pause() {
  }

  @Override
  public void resize(int w, int h) {
  }

  @Override
  public void show() {
  }

  public void new_game() {
    for (int i=0; i<100; i++) {
      zombies.get(i).reset();
    }
    survivor.reset();
  }

  public void game_over() {
    game.setScreen(new GameOverScreen(game, (last_millis - start_millis) / 1000));
  }

  void draw_avatar(Texture image, Vector3 pos) {
    game.res.batch.draw(image, pos.x - image.getWidth() / 2.0f, pos.y - image.getHeight() / 2.0f);
  }

  // have we been hit?
  void man_update() {
    Zombie z = null;
    for (int i=0; i<zombies.size; i++) {
      z = zombies.get(i);
      if (z.state == 2) {
	float d = z.pos.dst(survivor.pos);
	if (d < 32) {
	  z.shoot();
	  survivor.life -= 1;
	}
      }
    }

    Item item;
    for (int i=0; i<items.size; i++) {
      item = items.get(i);
      if (item.state == Item.ItemState.VISIBLE) {
	float d = item.pos.dst(survivor.pos);
 	if (d < 32) {
	  if (item.type == Item.ItemType.AMMO && survivor.ammo < 8) {
	    survivor.ammo += 1;
	    item.pickup();
	  }
	  else if (item.type == Item.ItemType.LIFE && survivor.life < 8) {
	    survivor.life += 1;
	    item.pickup();
	  }
	}
      }
    }

    if (survivor.life <= 0) {
      game_over();
    }

    // if (!map.inBounds(survivor.pos)) {
    //   game_over();
    // }
  }
}
