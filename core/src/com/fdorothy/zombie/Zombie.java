package com.fdorothy.zombie;

import com.badlogic.gdx.math.Vector3;

public class Zombie {
  public Vector3 pos;
  public Vector3 new_pos;
  public Vector3 delta;

  //  0 = void, 1 = spawning, 2 = alive, 3 = dead
  public int state;
  public float spawn_timer;
  public Map map;

  Zombie(Map map) {
    this.map = map;
    reset();
  }

  public void reset() {
    state = 0;
    spawn_timer = 0.0f;
    pos = new Vector3();
    delta = new Vector3();
    new_pos = new Vector3();
  }

  public void update(float dt) {
    if (spawn_timer > 0.0f) {
      spawn_timer -= dt;
    }
    else if (state == 1) {
      state = 2;
    } else if (state == 3) {
      state = 0;
    } else {
      float speedFactor = map.speedFactor(pos);
      new_pos.x = pos.x + delta.x*dt*speedFactor;
      new_pos.y = pos.y + delta.y*dt*speedFactor;
      if (map.inBounds(new_pos)) {
	pos.set(new_pos);
      }
    }
  }

  public void spawn(float x, float y) {
    pos.set(x, y, 0.0f);
    delta.set(0.0f, 0.0f, 0.0f);
    this.state = 1;
    this.spawn_timer = 3.0f;
  }

  public void shoot() {
    if (state == 2) {
      state = 3;
      spawn_timer = 10.0f;
      delta.set(0.0f,0.0f,0.0f);
    }
  }

  public void set_destination(float dx, float dy) {
    delta.set(dx,dy,0.0f);
  }
}
