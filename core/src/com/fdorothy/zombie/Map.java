 package com.fdorothy.zombie;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import java.lang.Float;

public class Map {
  public TiledMap map;
  public OrthogonalTiledMapRenderer renderer;
  public final float unitScale = 1.0f;
  public final float TILE_WIDTH = 32.0f;
  public TiledMapTileLayer pathLayer;
  public TiledMapTileLayer shrubLayer;
  public TiledMapTileLayer mountainLayer;
  public int width;
  public int height;

  public Map(TiledMap map) {
    this.map = map;
    pathLayer = (TiledMapTileLayer)map.getLayers().get("paths");
    shrubLayer = (TiledMapTileLayer)map.getLayers().get("shrubs");
    mountainLayer = (TiledMapTileLayer)map.getLayers().get("mountain");
    renderer = new OrthogonalTiledMapRenderer(this.map, unitScale);
    width = pathLayer.getWidth();
    height = pathLayer.getHeight();
  }

  public float speedFactor(Vector3 pos) {
    Cell cell = shrubLayer.getCell((int)(pos.x/TILE_WIDTH), (int)(pos.y/TILE_WIDTH));
    if (cell != null) {
      TiledMapTile tile = cell.getTile();
      MapProperties props = tile.getProperties();
      return props.get("speedFactor", 1.0f, Float.class);
    }
    return 1.0f;
  }

  public boolean inBounds(Vector3 pos) {
    return pos.x >= 0 && pos.y >= 0 && pos.x <= width * TILE_WIDTH && pos.y <= height * TILE_WIDTH;
  }
}
