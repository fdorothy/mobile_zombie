package com.fdorothy.zombie;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.math.Vector3;

public class ZombieGame extends Game {
  public Resources res;
  public Vector3 gps;

  @Override
  public void create () {
    res = new Resources();
    this.setScreen(new MainMenuScreen(this));
    gps = new Vector3();
  }
  
  @Override
  public void render () {
    super.render();
  }
	
  @Override
  public void dispose () {
    res.dispose();
  }
}
