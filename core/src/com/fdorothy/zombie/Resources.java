package com.fdorothy.zombie;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Resources {
  public Texture man;
  public Texture zombie;
  public Texture spawn;
  public Texture dead;
  public Texture gun;
  public Texture bullet;
  public Texture heart;
  public Texture bullet_piece;
  public Texture heart_piece;
  public TiledMap map;
  public SpriteBatch batch;
  public SpriteBatch batch_hud;
  public BitmapFont font;
  public int width, height;

  public Resources() {
    man = new Texture("man_piece.png");
    zombie = new Texture("zombie_piece.png");
    dead = new Texture("tombstone.png");
    bullet = new Texture("bullet.png");
    bullet_piece = new Texture("bullet_piece.png");
    heart = new Texture("heart.png");
    heart_piece = new Texture("heart_piece.png");
    map = new TmxMapLoader().load("rrpark.tmx");
    batch = new SpriteBatch();
    batch_hud = new SpriteBatch();
    font = new BitmapFont();
    width = Gdx.graphics.getWidth();
    height = Gdx.graphics.getHeight();
  }

  public void dispose() {
    man.dispose();
    zombie.dispose();
    dead.dispose();
    bullet.dispose();
    bullet_piece.dispose();
    heart.dispose();
    heart_piece.dispose();
    map.dispose();
    font.dispose();
    batch.dispose();
    batch_hud.dispose();
  }
}
