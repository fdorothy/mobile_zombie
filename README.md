# README #

This is the Mobile Zombie project. This is an urban zombie survival game. The current implementation only works for Railroad Park. If you attempt to play and you are not in Railroad park then you will immediately die. The Desktop version fakes your location to start in Railroad Park.

## Setup ##

This project uses the libgdx framework (https://libgdx.badlogicgames.com/). You can build this project for multiple targets, but only Android and the desktop versions are currently supported. The iOS code requires GPS information which is not yet implemented.

### Android ###

To build the project you first need to check out the code. The **important** thing to do after checking out the code is to modify where your Android SDK lives. This piece of information is in local.properties.


```
#!python

# change this to wherever you installed the Android SDK
sdk.dir=/home/fdorothy/local/android-sdk-linux
```


Next, execute the following from inside of the repository:


```
#!bash

./gradlew assembleDebug
```


If all goes well this should produce an APK that you can manually install on your Android device. You can also run the following to build and install on the Android device:


```
#!bash

./gradlew installDebug
```


Moving around inside of Railroad Park should update your position in the game. Tapping on zombies will shoot them.

## Desktop ##

The Desktop is preferred for testing out game mechanics and graphics. To run on the desktop you can simply run:


```
#!bash

./gradlew :desktop:run
```


This will compile the code and run the game inside of a window. You can press 'wasd' to move the survivor and click on the zombies to shoot them.